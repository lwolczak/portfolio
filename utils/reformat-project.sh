#!/bin/sh

# Absolute path to this script, e.g. /home/user/bin/rebuild.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPT_DIR=$(dirname "$SCRIPT")

cd $SCRIPT_DIR/..
REPO_ROOT=`pwd -P`
SOURCES="playground src tests/Assets tests/Utils"

for SOURCE in ${SOURCES}; do
  cd $REPO_ROOT/$SOURCE

  find -name *.hpp -or -name *.cc | cut --complement -c 1-2 | \
  while read filename; do
    clang-format -i --style=file:$REPO_ROOT/.clang-format $REPO_ROOT/$SOURCE/$filename
  done

done

