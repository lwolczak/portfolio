#include "portfolio.hpp"
#include <iostream>

using namespace std;

using namespace Assets;
using namespace Utils::ThreadPool;

void FillMatrixWithRandomValues(Matrix<long double> *matrix);

int main(int argc, char *argv[]) {
  ThreadPool::Initialize(std::thread::hardware_concurrency() - 1);
  auto pool = ThreadPool::Get();

  int matrixSize = 500;

  if (argc == 2) {
    matrixSize = std::stoi({argv[1]});
  }

  Matrix<long double> matrix1(matrixSize, matrixSize), matrix2(matrixSize, matrixSize);

  FillMatrixWithRandomValues(&matrix1);
  FillMatrixWithRandomValues(&matrix2);

  auto oneThreadStart = chrono::steady_clock::now();
  auto result1 = matrix1 * matrix2;
  auto oneThreadEnd = chrono::steady_clock::now();

  auto multiThreadStart = chrono::steady_clock::now();
  auto result2 = matrix1.multiThreadingMultiplyWith(matrix2, pool);
  auto multiThreadEnd = chrono::steady_clock::now();

  cout << "Matrix size: " << matrixSize << endl;

  cout << "Elapsed time in milliseconds (one thread):     " << chrono::duration_cast<chrono::milliseconds>(oneThreadEnd - oneThreadStart).count()
       << " ms" << endl;

  cout << "Elapsed time in milliseconds (multithreading): " << chrono::duration_cast<chrono::milliseconds>(multiThreadEnd - multiThreadStart).count()
       << " ms" << endl;

  pool->WaitForAllDone();
  pool->Stop();

  string strToTrim{"**ABCDEFG**"};
  cout << "----------------------------------------------------" << endl;
  cout << "Trim test: " << strToTrim << " -> " << trim_copy(strToTrim, '*') << endl;

  return 0;
}

void FillMatrixWithRandomValues(Matrix<long double> *matrix) {
  long double lower_bound = -1;
  long double upper_bound = 1;
  std::uniform_real_distribution<long double> unif(lower_bound, upper_bound);
  std::random_device r;
  std::default_random_engine re(std::default_random_engine::default_seed);

  for (std::size_t i = 0; i < matrix->rows(); i++)
    for (std::size_t j = 0; j < matrix->columns(); j++)
      (*matrix)(i, j) = unif(re);
}
