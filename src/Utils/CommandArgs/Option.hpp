#pragma once

#include "Argument.hpp"

namespace Utils::CommandArgs {
class Option : public Argument {
public:
  Option();

  ParseResultE Parse(const std::string &str) override;
};

} // namespace Utils::CommandArgs
