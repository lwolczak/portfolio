#include "Option.hpp"

#include "StringTrim.hpp"

namespace Utils::CommandArgs {
Option::Option() : Argument() {}

ParseResultE Option::Parse(const std::string &str) {
  this->sourceString = str;
  auto trimmedStr = std::trim_copy(str);

  if (trimmedStr == this->GetName()) {
    this->parsedValue = trimmedStr;
    return ParseResultE::Success;
  }

  return ParseResultE::Failure;
}

} // namespace Utils::CommandArgs
