#pragma once

#include <string>

namespace Utils::CommandArgs {
enum class ParseResultE {
  Failure = 0,
  Partial = 1,
  Success = 2,
};
}

std::string toString(const Utils::CommandArgs::ParseResultE &parseResult);
Utils::CommandArgs::ParseResultE fromString(const std::string parseResult) noexcept(false);
Utils::CommandArgs::ParseResultE fromInt(const int &parseResult) noexcept(false);
