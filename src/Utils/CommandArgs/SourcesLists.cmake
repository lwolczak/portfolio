
if (NOT DEFINED UtilsCommandArgsPath)
set (UtilsCommandArgsPath ".")
endif ()

set(CommandArgsSources ${CommandArgsSources} 
"${UtilsCommandArgsPath}/ItemIf.hpp" 
"${UtilsCommandArgsPath}/Argument.hpp" "${UtilsCommandArgsPath}/Argument.cc"
"${UtilsCommandArgsPath}/ArgParser.hpp" "${UtilsCommandArgsPath}/ArgParser.cc"
"${UtilsCommandArgsPath}/Option.hpp" "${UtilsCommandArgsPath}/Option.cc"
"${UtilsCommandArgsPath}/ParseResultE.hpp"
)

if (UNITTEST)
set(CommandArgsSources ${CommandArgsSources} 
"Utils/CommandArgs/ArgParserFixture.hpp" "Utils/CommandArgs/ArgParserFixture.cc"
"Utils/CommandArgs/ArgumentFixture.hpp" "Utils/CommandArgs/ArgumentFixture.cc"
"Utils/CommandArgs/ArgumentParsingFixture.hpp" "Utils/CommandArgs/ArgumentParsingFixture.cc"
"Utils/CommandArgs/OptionParsingFixture.hpp" "Utils/CommandArgs/OptionParsingFixture.cc"
)
endif ()