#pragma once

#include "ParseResultE.hpp"

#include <string>
#include <vector>

namespace Utils::CommandArgs {
class ItemIf {
public:
  virtual void SetName(const std::string &name) = 0;
  virtual std::string GetName() const = 0;

  virtual void SetDescription(const std::string &description) = 0;
  virtual std::string GetDescription() const = 0;

  virtual void AddExample(const std::string &example) = 0;
  virtual std::vector<std::string> GetExamples() const = 0;

  virtual std::string GetValue() const = 0;
  virtual ParseResultE Parse(const std::string &str) = 0;
};
} // namespace Utils::CommandArgs