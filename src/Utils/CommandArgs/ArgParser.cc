#include "ArgParser.hpp"

#include <algorithm>
#include <iostream>

#include "Option.hpp"

namespace Utils::CommandArgs {
ArgParser::ArgParser(const int argc, const char **argv) : argc(argc), argv(argv) { this->InsertHelpOptions(); }

void ArgParser::AddOption(ItemIf *option) {
  this->options.emplace_back(option);
  this->nameToOption.insert({option->GetName(), option});
  this->optionRequested.insert({option, false});
}

void ArgParser::AddArgument(ItemIf *argument) {}

const std::vector<std::string> &ArgParser::GetErrors() const { return this->parseErrors; }

bool ArgParser::HasErrors() const { return this->parseErrors.size() > 0; }

bool ArgParser::HasOption(ItemIf *option) {
  if (auto it = this->optionRequested.find(option); it != this->optionRequested.end()) {
    return this->optionRequested.at(option);
  }
  return false;
}

bool ArgParser::HasOption(const std::string &optionName) {
  if (auto it = this->nameToOption.find(optionName); it != this->nameToOption.end()) {
    return this->HasOption(this->nameToOption[optionName]);
  }
  return false;
}

bool ArgParser::IsHelpRequested() const { return this->helpRequested; }

bool ArgParser::Parse() {
  if (this->argc <= 1) {
    return true;
  }

  for (int i = 1; i < this->argc; i++) {
    std::string arg = this->GetArgument(i);

    { // Is current arg a help request?
      auto [result, item] = this->ParseSource(this->helpOptions, arg);
      if (result == ParseResultE::Success) {
        this->helpRequested = true;
        continue;
      }
    }

    { // Is current arg an option?
      auto [result, item] = this->ParseSource(this->options, arg);
      if (result == ParseResultE::Success) {
        this->helpRequested = true;
        continue;
      }
    }

    this->parseErrors.emplace_back(std::string{"Unknown option: '"} + arg + std::string{"'"});
  }

  return false;
}

void ArgParser::InsertHelpOptions() {
  std::vector<std::string> optionNames{"-h", "--h", "-help", "--help", "?", "-?", "--?"};

  for (const auto &name : optionNames) {
    auto option = new Option();
    option->SetName(name);

    this->helpOptions.emplace_back(option);
  }
}

std::pair<ParseResultE, ItemIf *> ArgParser::ParseSource(std::vector<ItemIf *> &source, const std::string &str) {
  for (ItemIf *item : source) {
    auto result = item->Parse(str);
    if (result != ParseResultE::Failure) {
      return std::make_pair(result, item);
    }
  }

  return std::make_pair(ParseResultE::Failure, nullptr);
}

std::string ArgParser::GetArgument(const int &index) const { return std::string(this->argv[index]); }
} // namespace Utils::CommandArgs