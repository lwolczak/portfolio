#pragma once

#include "ItemIf.hpp"
#include "ParseResultE.hpp"

#include <string>
#include <tuple>
#include <vector>

namespace Utils::CommandArgs {
class Argument : public ItemIf {
public:
  Argument();

  void SetName(const std::string &name) override;
  std::string GetName() const override;

  void SetDescription(const std::string &description) override;
  std::string GetDescription() const override;

  void AddExample(const std::string &example) override;
  std::vector<std::string> GetExamples() const override;

  ParseResultE Parse(const std::string &str) override;
  std::string GetValue() const override;

protected:
  std::tuple<std::string, std::string> SplitToNameAndValue(const std::string &str) const;
  std::string TrimValue(const std::string &sourceValue) const;

  std::string name;
  std::string description;
  std::vector<std::string> examples;

  std::string sourceString;
  std::string parsedValue;
};
} // namespace Utils::CommandArgs