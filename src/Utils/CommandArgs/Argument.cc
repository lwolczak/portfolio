#include "Argument.hpp"

#include <algorithm>
#include <functional>
#include <iostream>
#include <vector>

#include "StringTrim.hpp"

namespace Utils::CommandArgs {
Argument::Argument() {}

std::string Argument::GetName() const { return this->name; }

void Argument::SetName(const std::string &name) { this->name = name; }

std::string Argument::GetDescription() const { return this->description; }

void Argument::SetDescription(const std::string &description) { this->description = description; }

std::vector<std::string> Argument::GetExamples() const { return this->examples; }

void Argument::AddExample(const std::string &example) { this->examples.emplace_back(example); }

ParseResultE Argument::Parse(const std::string &str) {
  this->sourceString = str;
  auto trimmedStr = std::trim_copy(str);
  auto [n, v] = this->SplitToNameAndValue(trimmedStr);

  std::trim(n);
  v = this->TrimValue(v);

  if (n.empty() == false && v.empty() == false && n == this->GetName()) {
    this->parsedValue = v;
    return ParseResultE::Success;
  }

  if (trimmedStr == this->GetName()) {
    return ParseResultE::Partial;
  }

  return ParseResultE::Failure;
}

std::string Argument::GetValue() const { return this->parsedValue; }

std::tuple<std::string, std::string> Argument::SplitToNameAndValue(const std::string &str) const {
  std::string parsedName, parsedValue;

  auto posApostrophe = str.find("'", 0);
  auto posQuote = str.find("\"", 0);
  auto npos = std::string::npos;

  if (auto pos = str.find("="); pos != npos && (posApostrophe == npos || posApostrophe > pos) && (posQuote == npos || posQuote > pos)) {
    parsedName = str.substr(0, pos);
    parsedValue = str.substr(pos + 1);
  } else if (auto pos = str.find(" "); pos != npos && (posApostrophe == npos || posApostrophe > pos) && (posQuote == npos || posQuote > pos)) {
    parsedName = str.substr(0, pos);
    parsedValue = str.substr(pos + 1);
  }

  return std::make_tuple(parsedName, parsedValue);
}

std::string Argument::TrimValue(const std::string &sourceValue) const {
  std::string returnValue = sourceValue;
  std::string currentValue;
  std::vector<unsigned char> charactersToTrim{0, '\'', '"'};

  auto trimFunction = [&charactersToTrim](std::string src) -> std::string {
    for (char c : charactersToTrim) {
      std::trim(src, c);
    }
    return src;
  };

  do {
    currentValue = returnValue;
    returnValue = trimFunction(currentValue);
  } while (currentValue != returnValue);

  return returnValue;
}
} // namespace Utils::CommandArgs
