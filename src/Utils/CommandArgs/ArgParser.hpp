#pragma once

#include "ItemIf.hpp"

#include <map>
#include <string>
#include <vector>

namespace Utils::CommandArgs {

class ArgParser {
public:
  ArgParser(const int argc, const char **argv);

  void AddOption(ItemIf *option);
  void AddArgument(ItemIf *argument);

  const std::vector<std::string> &GetErrors() const;
  bool HasErrors() const;

  bool HasOption(ItemIf *option);
  bool HasOption(const std::string &optionName);

  bool IsHelpRequested() const;

  bool Parse();

protected:
  virtual void InsertHelpOptions();

  std::pair<ParseResultE, ItemIf *> ParseSource(std::vector<ItemIf *> &source, const std::string &str);

  [[nodiscard]] std::string GetArgument(const int &index) const;

private:
  std::vector<ItemIf *> helpOptions;
  bool helpRequested = false;

  std::vector<ItemIf *> options;
  std::map<std::string, ItemIf *> nameToOption;
  std::map<ItemIf *, bool> optionRequested;

  std::vector<ItemIf *> arguments;

  std::vector<std::string> parseErrors;

  std::string programName;
  const int argc;
  const char **argv;

#ifdef UNITTEST
  friend class ArgParserFixture;
#endif
};

} // namespace Utils::CommandArgs