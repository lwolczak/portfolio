#pragma once

#include <condition_variable>
#include <functional>
#include <mutex>
#include <queue>
#include <thread>
#include <vector>

namespace Utils::ThreadPool {
class ThreadPool {
public:
  static ThreadPool *Get() noexcept(false);
  static void Initialize(const unsigned int &maxThreads) noexcept(false);

  void AddJob(const std::function<void()> &job);

  bool IsBusy();
  void WaitForAllDone();
  void Stop();

private:
  static ThreadPool *instance;
  static unsigned int threadsCount;
  ThreadPool();

  void MainLoop(int threadIndex);
  inline void SetThreadWorkingStatus(const int &threadIndex, const bool &state);

  std::vector<std::thread> threads;

  std::vector<bool> isThreadWorking;
  std::mutex isThreadWorkingMutex;
  std::condition_variable isThreadWorkingCondition;

  std::queue<std::function<void()>> jobs;
  std::mutex jobsMutex;
  std::condition_variable jobsMutexCondition;

  bool shouldTerminate = false;
};

} // namespace Utils::ThreadPool