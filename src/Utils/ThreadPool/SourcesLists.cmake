
if (NOT DEFINED UtilsThreadpoolPath)
  set (UtilsThreadpoolPath ".")
endif ()

set(ThreadPoolSources ${ThreadPoolSources} 
  "${UtilsThreadpoolPath}/ThreadPool.hpp" 
  "${UtilsThreadpoolPath}/ThreadPool.cc"
)
  
if (UNITTEST)
set(ThreadPoolSources ${ThreadPoolSources} 
  "Utils/ThreadPool/ThreadPoolFixture.hpp" 
  "Utils/ThreadPool/ThreadPoolFixture.cc"
)
endif ()