#include "ThreadPool.hpp"

#include <algorithm>
#include <iostream>

namespace Utils::ThreadPool {
ThreadPool *ThreadPool::instance = nullptr;
unsigned int ThreadPool::threadsCount = 0;

ThreadPool *ThreadPool::Get() noexcept(false) {
  if (ThreadPool::instance == nullptr) {
    std::string msg = "Before getting ThreadPool instance run ThreadPool::Initialize(int maxThreads)";
    std::cerr << msg << std::endl;
    throw std::string(msg);
  }

  return ThreadPool::instance;
}

void ThreadPool::Initialize(const unsigned int &maxThreads) noexcept(false) {
  if (ThreadPool::instance != nullptr) {
    std::string msg = "ThreadPool already initialized";
    std::cerr << msg << std::endl;
    throw std::string(msg);
  }
  ThreadPool::threadsCount = maxThreads;
  ThreadPool::instance = new ThreadPool();
}

void ThreadPool::AddJob(const std::function<void()> &job) {
  std::unique_lock<std::mutex> lock(this->jobsMutex);
  this->jobs.push(job);
  lock.unlock();
  this->jobsMutexCondition.notify_one();
}

bool ThreadPool::IsBusy() {
  bool hasAnyJob;
  {
    std::unique_lock<std::mutex> lock(this->jobsMutex);
    hasAnyJob = !this->jobs.empty();
  }

  if (hasAnyJob)
    return true;

  bool isAnyWorking;
  {
    std::unique_lock<std::mutex> lock(this->isThreadWorkingMutex);
    auto countWorking = std::count(this->isThreadWorking.begin(), this->isThreadWorking.end(), true);
    isAnyWorking = (countWorking > 0);
  }

  return isAnyWorking;
}

void ThreadPool::WaitForAllDone() {
  std::mutex mutex;
  std::unique_lock<std::mutex> lock(mutex);
  this->isThreadWorkingCondition.wait(lock, [this]() -> bool { return !this->IsBusy(); });
}

void ThreadPool::Stop() {
  std::cout << "###---###> Stopping thread pool" << std::endl;
  {
    std::unique_lock<std::mutex> lock(this->jobsMutex);
    this->shouldTerminate = true;
  }
  this->jobsMutexCondition.notify_all();

  for (std::thread &t : this->threads) {
    t.join();
  }
  this->threads.clear();
  ThreadPool::instance = nullptr;
}

ThreadPool::ThreadPool() {
  std::cout << "###---###> Creating " << ThreadPool::threadsCount << " threads" << std::endl;

  this->threads.resize(ThreadPool::threadsCount);
  this->isThreadWorking.resize(ThreadPool::threadsCount);

  for (uint32_t i = 0; i < ThreadPool::threadsCount; i++) {
    this->threads.at(i) = std::thread(&ThreadPool::MainLoop, this, i);
    this->isThreadWorking.at(i) = false;
  }
}

void ThreadPool::MainLoop(int threadIndex) {
  while (true) {
    std::function<void()> jobToExecute;
    {
      std::unique_lock<std::mutex> lock(this->jobsMutex);
      this->jobsMutexCondition.wait(lock, [this]() -> bool { return this->jobs.empty() == false || this->shouldTerminate; });

      if (this->shouldTerminate) {
        return;
      }

      jobToExecute = this->jobs.front();
      this->jobs.pop();
    }

    SetThreadWorkingStatus(threadIndex, true);
    jobToExecute();
    SetThreadWorkingStatus(threadIndex, false);
    this->isThreadWorkingCondition.notify_all();
  }
}

void ThreadPool::SetThreadWorkingStatus(const int &threadIndex, const bool &state) {
  std::unique_lock<std::mutex> lock(this->isThreadWorkingMutex);
  this->isThreadWorking.at(threadIndex) = state;
}
} // namespace Utils::ThreadPool