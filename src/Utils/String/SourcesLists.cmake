
if (NOT DEFINED UtilsStringPath)
  set (UtilsStringPath ".")
endif ()

set(UtilsStringSources ${UtilsStringSources} 
  "${UtilsStringPath}/StringTrim.hpp" 
  "${UtilsStringPath}/StringTrim.cc"
)

if (UNITTEST)
set(UtilsStringSources ${UtilsStringSources} 
  "Utils/String/StringTrimFixture.hpp" 
  "Utils/String/StringTrimFixture.cc"
)
endif ()