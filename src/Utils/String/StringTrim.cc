#include "StringTrim.hpp"

namespace std {

void ltrim(string &s, const unsigned char &trimmedCharacter) {
  s.erase(s.begin(), find_if(s.begin(), s.end(), [trimmedCharacter](unsigned char ch) {
            if (trimmedCharacter > 0) {
              return ch != trimmedCharacter;
            }
            return !isspace(ch);
          }));
}

void rtrim(string &s, const unsigned char &trimmedCharacter) {
  s.erase(find_if(s.rbegin(), s.rend(),
                  [trimmedCharacter](unsigned char ch) {
                    if (trimmedCharacter > 0) {
                      return ch != trimmedCharacter;
                    }
                    return !isspace(ch);
                  })
              .base(),
          s.end());
}

void trim(string &s, const unsigned char &trimmedCharacter) {
  rtrim(s, trimmedCharacter);
  ltrim(s, trimmedCharacter);
}

string ltrim_copy(string s, const unsigned char &trimmedCharacter) {
  ltrim(s, trimmedCharacter);
  return s;
}

string rtrim_copy(string s, const unsigned char &trimmedCharacter) {
  rtrim(s, trimmedCharacter);
  return s;
}

string trim_copy(string s, const unsigned char &trimmedCharacter) {
  trim(s, trimmedCharacter);
  return s;
}

} // namespace std
