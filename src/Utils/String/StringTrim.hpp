#pragma once

#include <algorithm>
#include <cctype>
#include <locale>
#include <string>

namespace std {
void ltrim(string &s, const unsigned char &trimmedCharacter = 0);
void rtrim(string &s, const unsigned char &trimmedCharacter = 0);
void trim(string &s, const unsigned char &trimmedCharacter = 0);

string ltrim_copy(string s, const unsigned char &trimmedCharacter = 0);
string rtrim_copy(string s, const unsigned char &trimmedCharacter = 0);
string trim_copy(string s, const unsigned char &trimmedCharacter = 0);
} // namespace std
