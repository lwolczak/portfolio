# Portfolio - Tests and Code Coverage

## How to build:

### Automatic
```bash
# cd into tests catalog and execute
./rebuild.sh
```

### Manual
```bash
# cd into tests catalog and execute
mkdir -p build
cd build
cmake ..
make
make test
make coverage
make install
```

### Coverage files
After install coverage files are located inside ```out/coverage``` path in tests catalog. 


### Tests 
After install tests can be executed in out/bin catalog:
```bash
out/bin/Tests --gtest_filter=*
```
