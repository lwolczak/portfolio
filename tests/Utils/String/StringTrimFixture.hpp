#pragma once

#include <tuple>

#include <gtest/gtest.h>

#include "../../tests.hpp"
#include "StringTrim.hpp"

namespace Utils::String {
class LTrimStringFixture : public ::testing::TestWithParam<std::tuple<std::string, unsigned char, std::string>> {};

class RTrimStringFixture : public ::testing::TestWithParam<std::tuple<std::string, unsigned char, std::string>> {};

class TrimStringFixture : public ::testing::TestWithParam<std::tuple<std::string, unsigned char, std::string>> {};
} // namespace Utils::String