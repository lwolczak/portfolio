#include "StringTrimFixture.hpp"

namespace {
auto LTrimParams = ::testing::Values(std::make_tuple(std::string{"  some string"}, 0, std::string{"some string"}),
                                     std::make_tuple(std::string{"  some string  "}, 0, std::string{"some string  "}),
                                     std::make_tuple(std::string{"**some string"}, '*', std::string{"some string"}),
                                     std::make_tuple(std::string{"**some string**"}, '*', std::string{"some string**"}));

auto RTrimParams = ::testing::Values(std::make_tuple(std::string{"some string  "}, 0, std::string{"some string"}),
                                     std::make_tuple(std::string{"  some string  "}, 0, std::string{"  some string"}),
                                     std::make_tuple(std::string{"some string**"}, '*', std::string{"some string"}),
                                     std::make_tuple(std::string{"**some string**"}, '*', std::string{"**some string"}));

auto TrimParams = ::testing::Values(std::make_tuple(std::string{"  some string  "}, 0, std::string{"some string"}),
                                    std::make_tuple(std::string{"**some string**"}, '*', std::string{"some string"}));
} // namespace

namespace Utils::String {
TEST_P(LTrimStringFixture, TrimmingLeftSideOfString) {
  auto [src, trimmingCharacter, result] = GetParam();

  std::ltrim(src, trimmingCharacter);

  EXPECT_EQ(src, result);
}

TEST_P(LTrimStringFixture, TrimmingCopyLeftSideOfString_shouldNotChangeSource) {
  auto [src, trimmingCharacter, result] = GetParam();

  auto srcCopy = src;
  auto trimmedSrc = std::ltrim_copy(src, trimmingCharacter);

  EXPECT_EQ(trimmedSrc, result);
  EXPECT_EQ(srcCopy, src);
}

TEST_P(RTrimStringFixture, TrimmingRightSideOfString) {
  auto [src, trimmingCharacter, result] = GetParam();

  std::rtrim(src, trimmingCharacter);

  EXPECT_EQ(src, result);
}

TEST_P(RTrimStringFixture, TrimmingCopyRightSideOfString_shouldNotChangeSource) {
  auto [src, trimmingCharacter, result] = GetParam();

  auto srcCopy = src;
  auto trimmedSrc = std::rtrim_copy(src, trimmingCharacter);

  EXPECT_EQ(trimmedSrc, result);
  EXPECT_EQ(srcCopy, src);
}

TEST_P(TrimStringFixture, TrimmingBothSidesOfString) {
  auto [src, trimmingCharacter, result] = GetParam();

  std::trim(src, trimmingCharacter);

  EXPECT_EQ(src, result);
}

TEST_P(TrimStringFixture, TrimmingCopyBothSidesOfString_shouldNotChangeSource) {
  auto [src, trimmingCharacter, result] = GetParam();

  auto srcCopy = src;
  auto trimmedSrc = std::trim_copy(src, trimmingCharacter);

  EXPECT_EQ(trimmedSrc, result);
  EXPECT_EQ(srcCopy, src);
}

INSTANTIATE_TEST_SUITE_P(TestingLTrims, LTrimStringFixture, LTrimParams);
INSTANTIATE_TEST_SUITE_P(TestingRTrims, RTrimStringFixture, RTrimParams);
INSTANTIATE_TEST_SUITE_P(TestingTrims, TrimStringFixture, TrimParams);

} // namespace Utils::String