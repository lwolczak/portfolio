#pragma once
#include "../../tests.hpp"
#include "gtest/gtest.h"

#include "ArgParser.hpp"
#include <string>

namespace Utils::CommandArgs {

class ArgParserFixture : public ::testing::Test {
protected:
  ArgParser *CreateEmptyArgs();
  ArgParser *CreateEmptyArgsWithParams();
};

} // namespace Utils::CommandArgs
