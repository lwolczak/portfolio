#include "ArgumentFixture.hpp"

#include <string>
#include <vector>

namespace {
const std::string Name{"var"};
const std::string Description{"Lorem ipsum dolor sit amet"};
const std::string Example1{"Example1"};
const std::string Example2{"Example2"};
std::vector<std::string> Examples{Example1, Example2};
} // namespace

namespace Utils::CommandArgs {

TEST_F(ArgumentFixture, GetName_ShouldReturnPassedValue) {
  Argument arg;
  arg.SetName(Name);

  EXPECT_EQ(Name, arg.GetName());
}

TEST_F(ArgumentFixture, GetDescription_ShouldReturnPassedValue) {
  Argument arg;
  arg.SetDescription(Name);

  EXPECT_EQ(Name, arg.GetDescription());
}

TEST_F(ArgumentFixture, GetExamples_ShouldReturnInsertedValues) {
  Argument arg;
  arg.AddExample(Example1);
  arg.AddExample(Example2);

  EXPECT_EQ(arg.GetExamples(), Examples);
}

} // namespace Utils::CommandArgs