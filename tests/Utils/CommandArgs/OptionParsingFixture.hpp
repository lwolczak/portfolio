#pragma once

#include <string>
#include <tuple>

#include <gtest/gtest.h>

#include "../../tests.hpp"

namespace Utils::CommandArgs {
enum class ParseResultE;

class OptionParsingFixture : public ::testing::TestWithParam<std::tuple<std::string, std::string, ParseResultE>> {};
} // namespace Utils::CommandArgs
