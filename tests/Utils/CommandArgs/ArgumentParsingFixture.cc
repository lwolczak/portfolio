#include "ArgumentParsingFixture.hpp"

#include "Argument.hpp"

namespace {
std::string ArgumentName = "--argument";

auto ValidExamples = ::testing::Values(std::make_tuple(ArgumentName + "=someValue", "someValue", Utils::CommandArgs::ParseResultE::Success),
                                       std::make_tuple(ArgumentName + "='someValue'", "someValue", Utils::CommandArgs::ParseResultE::Success),
                                       std::make_tuple(ArgumentName + "=\"someValue\"", "someValue", Utils::CommandArgs::ParseResultE::Success),
                                       std::make_tuple(ArgumentName + " someValue", "someValue", Utils::CommandArgs::ParseResultE::Success),
                                       std::make_tuple(ArgumentName + " 'someValue'", "someValue", Utils::CommandArgs::ParseResultE::Success),
                                       std::make_tuple(ArgumentName + " \"someValue\"", "someValue", Utils::CommandArgs::ParseResultE::Success));

auto PartialValidExamples = ::testing::Values(std::make_tuple(ArgumentName, "", Utils::CommandArgs::ParseResultE::Partial));

auto InvalidExamples = ::testing::Values(std::make_tuple(ArgumentName + "XX=someValue", "", Utils::CommandArgs::ParseResultE::Failure),
                                         std::make_tuple(ArgumentName + "XX='someValue'", "", Utils::CommandArgs::ParseResultE::Failure),
                                         std::make_tuple(ArgumentName + "XX=\"someValue\"", "", Utils::CommandArgs::ParseResultE::Failure),
                                         std::make_tuple(ArgumentName + "XX someValue", "", Utils::CommandArgs::ParseResultE::Failure),
                                         std::make_tuple(ArgumentName + "XX 'someValue'", "", Utils::CommandArgs::ParseResultE::Failure),
                                         std::make_tuple(ArgumentName + "XX \"someValue\"", "", Utils::CommandArgs::ParseResultE::Failure),
                                         std::make_tuple(ArgumentName + "XX", "", Utils::CommandArgs::ParseResultE::Failure));
} // namespace

namespace Utils::CommandArgs {
TEST_P(ArgumentParsingFixture, ArgumentParsing) {
  auto testData = GetParam();
  std::string stringToParse = std::get<0>(testData);
  std::string parsedValue = std::get<1>(testData);
  ParseResultE parsingResult = std::get<2>(testData);

  Argument arg;
  arg.SetName(ArgumentName);
  auto result = arg.Parse(stringToParse);

  EXPECT_EQ(parsingResult, result);
  EXPECT_EQ(parsedValue, arg.GetValue());
}

INSTANTIATE_TEST_SUITE_P(ShouldReturnSuccess_WhenParsingStringIsValid, ArgumentParsingFixture, ValidExamples);

INSTANTIATE_TEST_SUITE_P(ShouldReturnPartial_WhenParsingStringHasOnlyName, ArgumentParsingFixture, PartialValidExamples);

INSTANTIATE_TEST_SUITE_P(ShouldReturnFailure_WhenParsingStringHasInvalidName, ArgumentParsingFixture, InvalidExamples);
} // namespace Utils::CommandArgs