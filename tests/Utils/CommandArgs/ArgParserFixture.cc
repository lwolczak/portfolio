#include "ArgParserFixture.hpp"

namespace {
const int EmptyParser_argc = 1;
const char *EmptyParams_argv[] = {"program"};
const char *HelpRequestParams_argv[] = {"program", "-h"};
const char *RequestOption_argv[] = {"program", "--option"};
const int RequestOption_argc = 2;
const std::string UnknownOptionErrorMessage{"Unknown option: '--option'"};
const std::string HelpMessageHeader{"-h, --h, -help, --help, ?, -? or --? shows this message."};
} // namespace

namespace Utils::CommandArgs {
ArgParser *ArgParserFixture::CreateEmptyArgs() { return new ArgParser(EmptyParser_argc, EmptyParams_argv); }

ArgParser *ArgParserFixture::CreateEmptyArgsWithParams() { return new ArgParser(RequestOption_argc, RequestOption_argv); }

TEST_F(ArgParserFixture, EmptyParser_shouldPrintSimpleHelpMessage) {
  auto parser = CreateEmptyArgs();
  parser->Parse();

  EXPECT_FALSE(parser->IsHelpRequested());
  EXPECT_FALSE(parser->HasErrors());

  testing::internal::CaptureStdout();
  std::string coutOutput = testing::internal::GetCapturedStdout();
}

TEST_F(ArgParserFixture, RequestingUnknownOption_ShouldGiveErrorMessage) {
  auto parser = CreateEmptyArgsWithParams();
  parser->Parse();
  std::vector<std::string> ErrorMessages{UnknownOptionErrorMessage};

  EXPECT_FALSE(parser->IsHelpRequested());
  EXPECT_TRUE(parser->HasErrors());
  EXPECT_EQ(parser->GetErrors(), ErrorMessages);
}

} // namespace Utils::CommandArgs
