#include "OptionParsingFixture.hpp"

#include "Option.hpp"

namespace {
std::string OptionName = "--option";

auto TestValues = ::testing::Values(std::make_tuple(OptionName, OptionName, Utils::CommandArgs::ParseResultE::Success),
                                    std::make_tuple(OptionName + "=someValue", "", Utils::CommandArgs::ParseResultE::Failure),
                                    std::make_tuple(OptionName + "x", "", Utils::CommandArgs::ParseResultE::Failure));

} // namespace

namespace Utils::CommandArgs {
TEST_P(OptionParsingFixture, OptionParsing) {
  auto testData = GetParam();
  std::string stringToParse = std::get<0>(testData);
  std::string parsedValue = std::get<1>(testData);
  ParseResultE parsingResult = std::get<2>(testData);

  Option opt;
  opt.SetName(OptionName);
  auto result = opt.Parse(stringToParse);

  EXPECT_EQ(parsingResult, result);
  EXPECT_EQ(parsedValue, opt.GetValue());
}

INSTANTIATE_TEST_SUITE_P(ShouldReturnExpectedValueForGivenData, OptionParsingFixture, TestValues);

} // namespace Utils::CommandArgs
