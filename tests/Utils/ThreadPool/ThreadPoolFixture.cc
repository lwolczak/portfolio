#include "ThreadPoolFixture.hpp"

namespace {
const std::string NotInitializedExceptionMessage{"Before getting ThreadPool instance run ThreadPool::Initialize(int maxThreads)"};
const std::string MultipleInitializeExceptionMessage{"ThreadPool already initialized"};
auto Job = []() -> void { std::this_thread::sleep_for(std::chrono::milliseconds(100)); };
} // namespace
//
namespace Utils::ThreadPool {

void ThreadPoolFixture::SetUp() {}

void ThreadPoolFixture::TearDown() {}

TEST_F(ThreadPoolFixture, whenCallingGetBeforeInitialize_shouldThrowException) {
  EXPECT_THROW(
      {
        try {
          auto *pool = ThreadPool::Get();
        } catch (const std::string &e) {
          EXPECT_EQ(e, NotInitializedExceptionMessage);
          ThreadPool::Get()->Stop();
          throw;
        }
      },
      std::string);
}

TEST_F(ThreadPoolFixture, whenMultipleCallInitialize_shouldThrowException) {
  EXPECT_THROW(
      {
        try {
          ThreadPool::Initialize(1u);
          ThreadPool::Initialize(2u);
        } catch (const std::string &e) {
          EXPECT_EQ(e, MultipleInitializeExceptionMessage);
          ThreadPool::Get()->Stop();
          throw;
        }
      },
      std::string);
}

TEST_F(ThreadPoolFixture, whenPoolIsEmpty_itShouldNotBeBusy) {
  ThreadPool::Initialize(2u);
  auto *pool = ThreadPool::Get();
  bool isBusy = pool->IsBusy();
  EXPECT_FALSE(isBusy);
  pool->Stop();
}

TEST_F(ThreadPoolFixture, whenPoolIsNotEmpty_itShouldBeBusy) {
  ThreadPool::Initialize(2u);
  auto *pool = ThreadPool::Get();

  pool->AddJob(Job);

  bool isBusy = pool->IsBusy();
  EXPECT_TRUE(isBusy);
  pool->Stop();
}

TEST_F(ThreadPoolFixture, whenPoolIsNotEmpty_waitShouldEnsureAllJobsAreFninished) {
  ThreadPool::Initialize(2u);
  auto *pool = ThreadPool::Get();

  pool->AddJob(Job);
  EXPECT_TRUE(pool->IsBusy());

  pool->WaitForAllDone();
  EXPECT_FALSE(pool->IsBusy());

  pool->Stop();
}
} // namespace Utils::ThreadPool