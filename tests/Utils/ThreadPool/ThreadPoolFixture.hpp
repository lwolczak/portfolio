#pragma once
#include "../../tests.hpp"
#include "gtest/gtest.h"

#include "ThreadPool.hpp"

namespace Utils::ThreadPool {
class ThreadPoolFixture : public ::testing::Test {
protected:
  void SetUp() override;
  void TearDown() override;
};
} // namespace Utils::ThreadPool