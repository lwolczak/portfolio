#!/bin/sh

# Absolute path to this script, e.g. /home/user/bin/rebuild.sh
SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPT_DIR=$(dirname "$SCRIPT")

cd $SCRIPT_DIR

# Create build directory if not exists and go into it
if [ ! -d build ] 
then
  mkdir build
fi
cd build

# Execute cmake and exit on failure
cmake ..
if [ $? -ne 0 ]
then 
  exit 1
fi

# Build all and exit on failure
make
if [ $? -ne 0 ]
then 
  exit 2
fi

# Generate data for coverage files
make test
if [ $? -ne 0 ]
then 
  exit 3
fi

# Generate coverage files
make coverage
if [ $? -ne 0 ]
then 
  exit 4
fi

# Install files
make install
if [ $? -ne 0 ]
then 
  exit 5
fi

# Print paths to user
CURRENT_DIR=$(dirname "$0")
BIN_DIR=$(dirname "$CURRENT_DIR/out/bin/Tests")
COVERAGE="$SCRIPT_DIR/out/coverage/index.html"

echo "---------------------------------------------"
echo "Code coverage files can be accessed in:"
echo "  $COVERAGE"
echo ""
echo "---------------------------------------------"
echo "Tests can be executed by running command:"
echo "  $BIN_DIR/Tests --gtest_filter=*"
echo ""