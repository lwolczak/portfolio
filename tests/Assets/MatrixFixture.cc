#include "MatrixFixture.hpp"

namespace Assets {
void MatrixFixture::SetUp() {}
void MatrixFixture::TearDown() {}

TEST_F(MatrixFixture, RowsAndColumns_ShouldMatchValuesFromConstructor) {
  const std::size_t ROWS = 7, COLS = 8;
  Matrix<int> m(ROWS, COLS);

  EXPECT_EQ(m.columns(), COLS);
  EXPECT_EQ(m.rows(), ROWS);
}
} // namespace Assets
