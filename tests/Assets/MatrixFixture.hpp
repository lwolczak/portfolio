#pragma once
#include "../tests.hpp"
#include "gtest/gtest.h"

#include "Matrix.hpp"

namespace Assets {
class MatrixFixture : public ::testing::Test {
protected:
  void SetUp() override;
  void TearDown() override;
};
} // namespace Assets