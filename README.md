# Portfolio

## How to build:

```bash
mkdir build
cd build
cmake ..
cd ..
cmake --build build --target install --config Debug

# All executables can be found in:
cd ../out/bin/

```

## Running tests
How to build and execute: [link](tests/README.md)